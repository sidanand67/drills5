let cards = [
    {
        id: 1,
        card_number: "5602221055053843723",
        card_type: "china-unionpay",
        issue_date: "5/25/2021",
        salt: "x6ZHoS0t9vIU",
        phone: "339-555-5239",
    },
    {
        id: 2,
        card_number: "3547469136425635",
        card_type: "jcb",
        issue_date: "12/18/2021",
        salt: "FVOUIk",
        phone: "847-313-1289",
    },
    {
        id: 3,
        card_number: "5610480363247475108",
        card_type: "china-unionpay",
        issue_date: "5/7/2021",
        salt: "jBQThr",
        phone: "348-326-7873",
    },
    {
        id: 4,
        card_number: "374283660946674",
        card_type: "americanexpress",
        issue_date: "1/13/2021",
        salt: "n25JXsxzYr",
        phone: "599-331-8099",
    },
    {
        id: 5,
        card_number: "67090853951061268",
        card_type: "laser",
        issue_date: "3/18/2021",
        salt: "Yy5rjSJw",
        phone: "850-191-9906",
    },
    {
        id: 6,
        card_number: "560221984712769463",
        card_type: "china-unionpay",
        issue_date: "6/29/2021",
        salt: "VyyrJbUhV60",
        phone: "683-417-5044",
    },
    {
        id: 7,
        card_number: "3589433562357794",
        card_type: "jcb",
        issue_date: "11/16/2021",
        salt: "9M3zon",
        phone: "634-798-7829",
    },
    {
        id: 8,
        card_number: "5602255897698404",
        card_type: "china-unionpay",
        issue_date: "1/1/2021",
        salt: "YIMQMW",
        phone: "228-796-2347",
    },
    {
        id: 9,
        card_number: "3534352248361143",
        card_type: "jcb",
        issue_date: "4/28/2021",
        salt: "zj8NhPuUe4I",
        phone: "228-796-2347",
    },
    {
        id: 10,
        card_number: "4026933464803521",
        card_type: "visa-electron",
        issue_date: "10/1/2021",
        salt: "cAsGiHMFTPU",
        phone: "372-887-5974",
    },
];


function cardsSumOdd(){
    let result = []; 
    cards.filter(card => {
        let sum = Array.from(card.card_number).reduce((total, curr, index) => {
            if (index%2 === 1){
                total += Number(curr); 
            }
            return total; 
        }, 0); 
        if(sum % 2 === 1){
            result.push(card.card_number); 
        }
    }); 
    return result; 
}

function cardsBeforeJune(){
    return cards.filter(card => {
        let cardMonth = parseInt(card.issue_date.split('/')[0]); 
        if(cardMonth < 6){
            return card; 
        }
    }); 
}

function addCVV(){
    return cards.map(card => {
        let number = Math.floor(Math.random() * 900 + 100); 
        card['CVV'] = number; 
        return card; 
    }); 
}

function addCardValidField(){
    return cards.map(card => {
        card.is_valid = null;
        return card;  
    }); 
}

function makeCardsInvalidBeforeMarch(){
    return cards.map(card => {
        let month = parseInt(card.issue_date.split('/')[0]); 
        if (month < 3){
            card.is_valid = false; 
        }
        else {
            card.is_valid = true; 
        }
        return card; 
    })
}

function groupByMonth(){
    let months = [
        'January', 
        'February', 
        'March', 
        'April', 
        'May', 
        'June', 
        'July',
        'August', 
        'September', 
        'October', 
        'November',
        'December'
    ]; 

    return cards.reduce((acc, curr) => {
        let cardMonth = parseInt(curr.issue_date.split('/')[0]);
        if(acc[months[cardMonth-1]]){
            acc[months[cardMonth-1]].push(curr); 
        }
        else {
            acc[months[cardMonth-1]] = [curr]; 
        }
        return acc;
    }, []); 
}

function sortByIssueDate(){
    cards.sort((card1,card2) => {
        card1Date = card1.issue_date.split('/').map(element => parseInt(element)); 
        card2Date = card2.issue_date.split('/').map(element => parseInt(element)); 
        if(card1Date[0] > card2Date[0]){
            return 1; 
        }
        else if(card1Date[0] < card2Date[0]){
            return -1; 
        }
        else {
            if(card1Date[1] > card2Date[1]){
                return 1; 
            }
            else if(card1Date[1] < card2Date[1]){
                return -1; 
            }
            else {
                return 0; 
            }
        }
    }); 
}

// Function Calls 
let cardsEvnPosSumOdd = cardsSumOdd(); 
console.log(cardsEvnPosSumOdd); 

let cardsBeforeJuneList = cardsBeforeJune(); 
console.log(cardsBeforeJuneList); 

cards = addCVV(); 
console.log(cards); 

cards = addCardValidField(); 
console.log(cards); 

cards = makeCardsInvalidBeforeMarch(); 
console.log(cards); 

let cardsGroupedByMonths = groupByMonth();
console.log(cardsGroupedByMonths); 

sortByIssueDate(); 
console.log(cards); 